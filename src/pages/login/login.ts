import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { HomePage } from '../home/home';
import { LostpwdPage } from '../lostpwd/lostpwd';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

	email: string;
	password: string;
	loginForm: FormGroup;

  constructor(
  	public navCtrl: NavController,
  	public navParams: NavParams,
  	public formBuilder: FormBuilder) {

  		this.loginForm = this.formBuilder.group({
			password: new FormControl('', Validators.compose([
				Validators.required,
				Validators.minLength(6),
				Validators.maxLength(30),
				Validators.pattern('^(?=.*[a-z])(?=.*[A-Z]])(?=.*[0-9])[a-zA-Z0-9]+$')
			])),
			email: new FormControl('', Validators.compose([
				Validators.required,
				Validators.minLength(6),
				Validators.maxLength(50),
				Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
			]))
		});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }


	login(){
		console.log("Usuario: " + this.email);
		console.log("Contraseña: " + this.password);
		this.navCtrl.push(HomePage);
	}

	lostPwd() {
		this.navCtrl.push(LostpwdPage);
	}

}








		// this.loginForm = this.formBuilder.group({
		// 	password: new FormControl('', Validators.compose([
		// 		Validators.required,
		// 		Validators.minLength(6),
		// 		Validators.maxLength(30),
		// 		Validators.pattern('^(?=.*[a-z])(?=.*[A-Z]])(?=.*[0-9])[a-zA-Z0-9]+$')
		// 	])),
		// 	email: new FormControl('', Validators.compose([
		// 		Validators.required,
		// 		Validators.minLength(6),
		// 		Validators.maxLength(50),
		// 		Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
		// 	]))
		// });

	

	// lostPwd() {
	// 	this.router.navigate(['/lost'])
	// 	// this.navCtrl.navigateRoot(LostPwdPagePage);
	// }


