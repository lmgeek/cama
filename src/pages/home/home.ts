import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { AlertController, LoadingController } from 'ionic-angular';

import { InAppBrowser } from '@ionic-native/in-app-browser';

import { File } from '@ionic-native/file';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer';
import { FileTransfer } from '@ionic-native/file-transfer';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  loading: any;

  constructor(
  	public navCtrl: NavController,
  	private iab: InAppBrowser,
    private document: DocumentViewer,
    private file: File,
    private transfer: FileTransfer,
    private platform: Platform,
    private alertCtrl: AlertController,
    public loadingCtrl: LoadingController
  	) {

  }

  // const fileTransfer: FileTransferObject = this.transfer.create();

  openLocalPdf() {
    const options: DocumentViewerOptions = {
      title: 'Catalogo'
    }
    this.document.viewDocument('assets/cencosud-2018.pdf', 'application/pdf', options);
  }

//Mostrar pdf online
  downloadAndOpenPdf() {
    let path = null;
 
    if (this.platform.is('ios')) {
      path = this.file.documentsDirectory;
    } else if (this.platform.is('android')) {
      path = this.file.dataDirectory;
    }
     
    this.showLoader();
    const transfer = this.transfer.create();
    transfer.download('http://paihuano.travel/celta/catalogo.pdf', path + 'catalogo.pdf').then(entry => {
      let url = entry.toURL();
      this.document.viewDocument(url, 'application/pdf', {});
      this.loading.dismiss();
    });
    // this.presentAlert();

  }

  // download() {
  // const url = 'http://www.example.com/file.pdf';
  // fileTransfer.download(url, this.file.dataDirectory + 'file.pdf').then((entry) => {
  //   console.log('download complete: ' + entry.toURL());
  // }, (error) => {
  //   // handle error
  // });
  // }

//Ver PDF con google docs
  showPDF(){
    let url = encodeURI('https://puertocreativo.cl/celta/catalogo.pdf');
  	this.iab.create('https://docs.google.com/viewer?url=' + url);
  }

//Alertas popup
  presentAlert() {
    let alert = this.alertCtrl.create({
      title: 'Cargando Catalogo',
      subTitle: 'El catalogo se abrira en unos instantes, aguarde!',
      buttons: ['Cerrar']
    });
    alert.present();
  }

//Loader
  showLoader(){
    this.loading = this.loadingCtrl.create({
      content: "El catalogo se abrira en unos instantes, se esta previsualizando. Por favor aguarde!..."
    });

    this.loading.present();
  }

//loading de prueba
  presentLoadingCustom() {
    let loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `
        <div class="custom-spinner-container">
          <div class="custom-spinner-box"></div>
        </div>`,
      duration: 5000
    });

    loading.onDidDismiss(() => {
      console.log('Dismissed loading');
    });

    loading.present();
  }
  //end loading

}
