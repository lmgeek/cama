import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login';

/**
 * Generated class for the LostpwdPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-lostpwd',
  templateUrl: 'lostpwd.html',
})
export class LostpwdPage {

	email: string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LostpwdPage');
  }

  lostpwd(){
		console.log("Usuario: " + this.email);
		this.navCtrl.push(LoginPage);
	}

}
