import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LostpwdPage } from './lostpwd';

@NgModule({
  declarations: [
    LostpwdPage,
  ],
  imports: [
    IonicPageModule.forChild(LostpwdPage),
  ],
})
export class LostpwdPageModule {}
